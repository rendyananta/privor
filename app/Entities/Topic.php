<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = "topics";
    protected $fillable = [
        'name',
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
