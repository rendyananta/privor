<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = [
        'title', 'description', 'price', 'banner'
    ];

    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class);
    }
}
