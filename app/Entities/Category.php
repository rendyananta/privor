<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $fillable = [
        'name', 'icon'
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class);
    }
}
