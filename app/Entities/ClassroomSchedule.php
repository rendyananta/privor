<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ClassroomSchedule extends Pivot
{
    protected $table = 'classroom_schedules';

    public function lectureAvailableSchedule()
    {
        return $this->belongsTo(LecturerAvailableSchedule::class);
    }
}
