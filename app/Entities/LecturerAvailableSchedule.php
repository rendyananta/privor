<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class LecturerAvailableSchedule extends Model
{
    protected $table = 'lecturer_available_schedules';
    protected $fillable = [
        'day', 'time_from', 'time_to'
    ];
    public $timestamps = false;

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class);
    }
}
