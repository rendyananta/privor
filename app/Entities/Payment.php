<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = [
        'proof', 'status', 'paid_at'
    ];

    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }

    public function upgradeStatus()
    {
        $newStatus = $this->getAttribute('status');
        if ($this->getAttribute('status') == 'Menunggu pembayaran') {
            $newStatus = 'Menunggu verifikasi';
        } else if ($this->getAttribute('status') == 'Menunggu verifikasi') {
            $newStatus = 'Dibayar';
        }

        return $this->update(['status' => $newStatus]);
    }

    public function downgradeStatus()
    {
        $newStatus = $this->getAttribute('status');
        if ($this->getAttribute('status') == 'Dibayar' ) {
            $newStatus = 'Menunggu verifikasi';
        } else if ($this->getAttribute('status') == 'Menunggu verifikasi') {
            $newStatus = 'Menunggu pembayaran';
        }

        return $this->update(['status' => $newStatus]);
    }
}
