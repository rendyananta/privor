<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ClassroomPresence extends Model
{
    protected $table = 'classroom_presences';
    protected $fillable = [
        'time', 'status'
    ];
}
