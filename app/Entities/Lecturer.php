<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    protected $table = "lecturers";
    protected $fillable = [
        'rating', 'cv', 'is_verified', 'verified_at'
    ];

    public function availableSchedules()
    {
        return $this->hasMany(LecturerAvailableSchedule::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function verif()
    {
        $this->update(['is_verified' => true]);
    }

    public function unverif()
    {
        $this->update(['is_verified' => false]);
    }
}
