<?php
/**
 * @author<rendy> at 08/02/2019
 */

namespace App\Http\Controllers;


class CoursesController extends Controller
{

    public function index()
    {
        return view('courses.index');
    }

    public function show()
    {
        return view('courses.show');
    }

    public function pay()
    {
        return view('courses.paying_method');
    }

    public function start()
    {
        return view('courses.start');
    }

}