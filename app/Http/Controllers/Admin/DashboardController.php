<?php
/**
 * @author<rendy> at 25/01/2019
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index()
    {
        return view('admin.dashboard');
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $payload = $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6'
        ]);

        return auth('admin')->attempt($payload)
            ? redirect()->route('admin.dashboard.index')
            : redirect()->back()->withErrors('Username atau password yang dimasukkan salah');
    }

}