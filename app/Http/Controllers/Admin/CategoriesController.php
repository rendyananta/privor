<?php
/**
 * @author<rendy> at 03/02/2019
 */

namespace App\Http\Controllers\Admin;


use App\DataTables\CategoriesDataTable;
use App\Entities\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    public function index(CategoriesDataTable $dataTable)
    {
        return $dataTable->render('admin.categories.index');
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'icon' => 'image'
        ]);

        $category = new Category();
        if ($request->hasFile('icon')) {
            $category->fill([
                'name' => $request->input('name'),
                'icon' => $request->file('icon')->store('icons')
            ]);
        } else {
            $category->fill($request->only('name'));
        }

        $category->save();

        return $category->save()
            ? redirect()->route('admin.categories.index')->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function show(Category $category)
    {
        return view('admin.categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'icon' => 'photo'
        ]);
        if ($request->hasFile('icon')) {
            $category->fill([
                'name' => $request->input('name'),
                'icon' => $request->file('icon')->store('icons')
            ]);
        } else {
            $category->fill($request->only('name'));
        }
        $category->save();

        return $category->save()
            ? redirect()->route('admin.categories.index')->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function destroy(Category $category)
    {
        return $category->delete()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

}