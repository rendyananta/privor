<?php
/**
 * @author<rendy> at 06/02/2019
 */

namespace App\Http\Controllers\Admin;


use App\DataTables\UsersDataTable;
use App\Entities\User;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('admin.users.index');
    }

    public function destroy(User $user)
    {
        return $user->delete()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

}