<?php
/**
 * @author<rendy> at 03/02/2019
 */

namespace App\Http\Controllers\Admin;


use App\DataTables\FaqsDataTable;
use App\Entities\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    public function index(FaqsDataTable $dataTable)
    {
        return $dataTable->render('admin.faqs.index');
    }

    public function create()
    {
        return view('admin.faqs.create');
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'question' => 'required',
            'answer' => 'required'
        ]);

        $faq = new Faq();
        $faq->fill($data);
        $faq->save();

        return $faq->save()
            ? redirect()->route('admin.faqs.index')->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function show(Faq $faq)
    {
        return view('admin.faqs.show', compact('faq'));
    }

    public function edit(Faq $faq)
    {
        return view('admin.faqs.edit', compact('faq'));
    }

    public function update(Request $request, Faq $faq)
    {
        $data = $this->validate($request, [
            'question' => 'required',
            'answer' => 'required'
        ]);

        $faq->fill($data);
        $faq->save();

        return $faq->save()
            ? redirect()->route('admin.faqs.index')->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function destroy(Faq $faq)
    {
        return $faq->delete()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }
}