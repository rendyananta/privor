<?php
/**
 * @author<rendy> at 06/02/2019
 */

namespace App\Http\Controllers\Admin;


use App\DataTables\PaymentsDataTable;
use App\DataTables\UnverifiedPaymentsDataTable;
use App\DataTables\WaitingPaymentsDataTable;
use App\Entities\Payment;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{

    public function index(PaymentsDataTable $dataTable)
    {
        return $dataTable->render('admin.payments.index');
    }

    public function unverified(UnverifiedPaymentsDataTable $dataTable)
    {
        return $dataTable->render('admin.payments.index_unverified');
    }

    public function pending(WaitingPaymentsDataTable $dataTable)
    {
        return $dataTable->render('admin.payments.index_pending');
    }

    public function verif(Payment $payment)
    {
        return $payment->upgradeStatus()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function unverif(Payment $payment)
    {
        return $payment->downgradeStatus()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function destroy(Payment $payment)
    {
        return $payment->delete()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

}