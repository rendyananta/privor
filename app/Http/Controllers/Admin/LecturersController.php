<?php
/**
 * @author<rendy> at 06/02/2019
 */

namespace App\Http\Controllers\Admin;


use App\DataTables\LecturersDataTable;
use App\DataTables\UnverifiedLecturersDataTable;
use App\Entities\Lecturer;
use App\Http\Controllers\Controller;

class LecturersController extends Controller
{

    public function index(LecturersDataTable $dataTable)
    {
        return $dataTable->render('admin.lecturers.index');
    }

    public function unverified(UnverifiedLecturersDataTable $dataTable)
    {
        return $dataTable->render('admin.lecturers.index_unverified');
    }

    public function verif(Lecturer $lecturer)
    {
        return $lecturer->verif()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function unverif(Lecturer $lecturer)
    {
        return $lecturer->unverif()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

    public function destroy(Lecturer $lecturer)
    {
        return $lecturer->delete()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

}