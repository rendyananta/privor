<?php
/**
 * @author<rendy> at 06/02/2019
 */

namespace App\Http\Controllers\Admin;


use App\DataTables\CoursesDataTable;
use App\Entities\Course;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{

    public function index(CoursesDataTable $dataTable)
    {
        return $dataTable->render('admin.courses.index');
    }

    public function destroy(Course $course)
    {
        return $course->delete()
            ? redirect()->back()->with('Success')
            : redirect()->back()->withErrors('Internal Server Error');
    }

}