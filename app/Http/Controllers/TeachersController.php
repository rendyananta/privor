<?php
/**
 * @author<rendy> at 08/02/2019
 */

namespace App\Http\Controllers;


class TeachersController extends Controller
{

    public function index()
    {
        return view('teachers.main');
    }

    public function show()
    {
        return view('teachers.show');
    }

}