<?php
/**
 * @author<rendy> at 08/02/2019
 */

namespace App\Http\Controllers;


class HomeController extends Controller
{

    public function index()
    {
        return view('landing');
    }

    public function about()
    {
        return view('contacts.about');
    }

}