<?php

namespace App\DataTables;

use App\Entities\Category;
use Yajra\DataTables\Services\DataTable;

class CategoriesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->escapeColumns()
            ->editColumn('icon', function (Category $category) {
                if (isset($category->icon)) {
                    $src = storage_asset($category->icon);
                    return "<img src='{$src}' width='30px'>";
                }
                return "Tidak ada gambar"
;            })
            ->rawColumns(['icon', 'action'])
            ->addColumn('action', 'admin.categories.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Entities\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '150px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => [
                'defaultContent' => '',
                'data'           => 'DT_Row_Index',
                'name'           => 'DT_Row_Index',
                'title'          => '#',
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
            'name',
            'icon',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Categories_' . date('YmdHis');
    }
}
