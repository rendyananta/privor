<?php
/**
 * @author<rendy> at 06/02/2019
 */

namespace App\DataTables;


use App\Entities\Course;
use Yajra\DataTables\Services\DataTable;

class CoursesDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->escapeColumns()
            ->addColumn('action', 'admin.categories.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Entities\Course $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Course $model)
    {
        return $model->newQuery()->with('lecture.user');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '150px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => [
                'defaultContent' => '',
                'data'           => 'DT_Row_Index',
                'name'           => 'DT_Row_Index',
                'title'          => '#',
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
            'title',
            'price',
            'lecturer' => [
                'data' => 'lecturer.user.name'
            ],
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Courses_' . date('YmdHis');
    }

}