<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerAvailableSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_available_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lecturer_id')->unsigned();
            $table->enum('day', [
                'Senin', 'Selasa', "Rabu", 'Kamis', 'Jumat', 'Sabtu', 'Minggu'
            ]);
            $table->time('time_from');
            $table->time('time_to');
            $table->foreign('lecturer_id')->references('id')
                ->on('lecturers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_available_schedules');
    }
}
