<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomPresencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_presences', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('time');
            $table->integer('classroom_id')->unsigned();
            $table->enum('lecture_status', [
                'Tidak hadir', 'Hadir', 'Tidak ada tanggapan',
            ]);
            $table->enum('student_status', [
                'Tidak hadir', 'Hadir', 'Tidak ada tanggapan',
            ]);
            $table->timestamps();

            $table->foreign('classroom_id')->references('id')
                    ->on('classrooms')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classroom_presences');
    }
}
