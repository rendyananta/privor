<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classroom_id')->unsigned();
            $table->integer('lecturer_available_schedules_id')->unsigned();

            $table->foreign('lecturer_available_schedules_id')->references('id')
                ->on('lecturer_available_schedules')->onDelete('no action');
            $table->foreign('classroom_id')->references('id')
                ->on('classrooms')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classroom_schedules');
    }
}
