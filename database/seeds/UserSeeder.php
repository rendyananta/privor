<?php
/**
 * @author<rendy> at 02/02/2019
 */

class UserSeeder extends DatabaseSeeder
{
    public function run()
    {
        $admin = new \App\Entities\Admin();
        $admin->fill([
            'username' => 'root',
            'password' => bcrypt('password')
        ]);
        $admin->save();
    }
}