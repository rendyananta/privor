@servers(['web' => 'deployer@167.99.74.93'])

@setup
    $repository = 'git@gitlab.com:rendyyangasli/privor.git';
    $releases_dir = '/var/www/privor/releases';
    $app_dir = '/var/www/privor';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    prepare
    update_symlinks
    set
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $releases_dir }}
    git fetch origin {{ $branch }}
    git reset --hard {{ $commit }}
@endtask

@task('prepare')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}

    yarn
    echo 'linking node_modules'
    ln -nfs {{ $app_dir }}/node_modules {{ $new_release_dir }}/node_modules

    composer install --prefer-dist --no-scripts -q -o
    echo 'linking vendor'
    ln -nfs {{ $app_dir }}/vendor {{ $new_release_dir }}/vendor
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    rm -rf {{ $app_dir }}/current
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('set')
    echo 'Getting your application ready'
    cd {{ $new_release_dir }}

    echo 'Clearing caches'
    php artisan cache:clear
    php artisan view:clear
    php artisan config:cache

    echo 'Compiling assets'
    npm run production

    echo 'Linking public storage'
    php artisan storage:link

    echo 'Migrating'
    php artisan migrate --force
@endtask
