<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'home.'], function () {
    Route::get('/', 'HomeController@index')->name('landing');
    Route::get('/tentang', 'HomeController@about')->name('about');
});

Route::group(['prefix' => 'kelas', 'as' => 'courses.'], function () {
    Route::get('/', 'CoursesController@index')->name('index');
    Route::get('/detil-kelas', 'CoursesController@show')->name('show');
    Route::get('/pay', 'CoursesController@pay')->name('pay');
    Route::get('/start', 'CoursesController@start')->name('start');
});

Route::group(['prefix' => 'pengajar', 'as' => 'teachers.'], function () {
    Route::get('/', 'TeachersController@index')->name('index');
    Route::get('profil', 'TeachersController@show')->name('show');
});

// Admin Routes
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {

    Route::group(['middleware' => 'guest:admin'], function () {
        Route::get('/login', 'DashboardController@showLoginForm')->name('login.form');
        Route::post('/login', 'DashboardController@login')->name('login');
    });

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard.index');

        Route::group(['prefix' => 'kategori', 'as' => 'categories.'], function () {
            Route::get('/', 'CategoriesController@index')->name('index');
            Route::get('/create', 'CategoriesController@create')->name('create');
            Route::post('/', 'CategoriesController@store')->name('store');
            Route::get('/{category}', 'CategoriesController@show')->name('show');
            Route::get('/{category}/edit', 'CategoriesController@edit')->name('edit');
            Route::put('/{category}', 'CategoriesController@update')->name('update');
            Route::delete('/{category}', 'CategoriesController@destroy')->name('destroy');
        });

        Route::group(['prefix' => 'tanya-jawab', 'as' => 'faqs.'], function () {
            Route::get('/', 'FaqsController@index')->name('index');
            Route::get('/create', 'FaqsController@create')->name('create');
            Route::post('/', 'FaqsController@store')->name('store');
            Route::get('/{faq}', 'FaqsController@show')->name('show');
            Route::get('/{faq}/edit', 'FaqsController@edit')->name('edit');
            Route::put('/{faq}', 'FaqsController@update')->name('update');
            Route::delete('/{faq}', 'FaqsController@destroy')->name('destroy');
        });

        Route::group(['prefix' => 'kelas', 'as' => 'courses.'], function () {
            Route::get('/', 'CoursesController@index')->name('index');
            Route::delete('/{course}', 'CoursesController@destroy')->name('destroy');
        });

        Route::group(['prefix' => 'pengguna', 'as' => 'users.'], function () {
            Route::get('/', 'UsersController@index')->name('index');
            Route::get('/{user}', 'UsersController@destroy')->name('destroy');
        });

        Route::group(['prefix' => 'pengajar', 'as' => 'lecturers.'], function () {
            Route::get('/', 'LecturersController@index')->name('index');
            Route::get('/belum-terverifikasi', 'LecturersController@unverified')->name('unverified');
            Route::patch('/{user}/verif', 'LecturersController@verif')->name('verif');
            Route::patch('/{user}/unverif', 'LecturersController@unverif')->name('unverif');
            Route::delete('/{user}', 'LecturersController@destroy')->name('destroy');
        });

        Route::group(['prefix' => 'transaksi', 'as' => 'payments.'], function () {
            Route::get('/', 'PaymentsController@index')->name('index');
            Route::get('/belum-terverifikasi', 'PaymentsController@unverified')->name('unverified');
            Route::get('/menunggu-pembayaran', 'PaymentsController@pending')->name('pending');
            Route::patch('/{payment}/verif', 'PaymentsController@verif')->name('verif');
            Route::patch('/{payment}/unverif', 'PaymentsController@unverif')->name('unverif');
            Route::delete('/{payment}', 'PaymentsController@destroy')->name('destroy');
        });

    });
});