@extends('layout.app')

@section('page_title', "Home")

@section('content')
    <section class="probootstrap-section probootstrap-section-colored" id="udx_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1 id="heading">Kelas</h1>
                </div>
                <div class="col-xs-4 section-heading probootstrap-animate" id="urut">
                    <p>Urutkan berdasarkan :</p>
                </div>
                <div class="col-xs-4 section-heading ">
                    <div class="dropdown">
                        <select class="dropbtn dropdown-toggle probootstrap-animate">
                            <option value="a">Terpopuler</option>
                            <option value="b">A-Z</option>
                            <option value="c">Rating Tertinggi</option>
                            <option value="d">Harga Terendah</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="{{ route('courses.show') }}">
                        <div class="probootstrap-service-2 text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_1.jpg" class="img-responsive">
                            </figure>
                            <div class="text">
                                <h5><b>Membuat Website dengan Laravel</b></h5>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star "></span>
                                <p>Rio Ikhsan</p>
                                <span class="label success"> <10  Topik </span>
                                <span class="label plain">Rp. 425.000</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="{{ route('courses.show') }}">
                        <div class="probootstrap-service-2 text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_1.jpg" class="img-responsive">
                            </figure>
                            <div class="text">
                                <h5><b>Belajar Java mudah dan cepat</b></h5>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star "></span>
                                <p>Rio Ikhsan</p>
                                <span class="label danger"> 10-14  Topik </span>
                                <span class="label plain">Rp. 425.000</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="{{ route('courses.show') }}">
                        <div class="probootstrap-service-2 text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_1.jpg" class="img-responsive">
                            </figure>
                            <div class="text">
                                <h5><b>Membuat Website dengan CodeIgniter</b></h5>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star "></span>
                                <p>Rio Ikhsan</p>
                                <span class="label warning"> 15-20  Topik </span>
                                <span class="label plain">Rp. 425.000</span>
                            </div>
                        </div>
                    </a>
                </div>

                @for ($i = 0; $i < 8; $i++)
                    <div class="col-md-3 col-sm-6">
                        <a href="{{ route('courses.show') }}">
                            <div class="probootstrap-service-2 text-center probootstrap-animate">
                                <figure class="media">
                                    <img src="img/person_1.jpg" class="img-responsive">
                                </figure>
                                <div class="text">
                                    <h5><b>Prinsip Desain UI/UX Aplikasi Edukasi</b></h5>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star "></span>
                                    <p>Rio Ikhsan</p>
                                    <span class="label info"> >20  Topik </span>
                                    <span class="label plain">Rp. 425.000</span>
                                </div>
                            </div>
                        </a>
                    </div>
                @endfor

                <div class="clearfix visible-sm-block visible-xs-block"></div>
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

@endsection