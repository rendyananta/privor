@extends('layout.app')

@section('page_title', "Mulai Belajar")

@section('content')

    <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1 id="head">Topik 2 : Setting Environment</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="probootstrap-flex-block">
                        <div class="probootstrap-text probootstrap-animate">
                            <div class="text-uppercase probootstrap-uppercase">Topik dalam Kelas</div>
                            <ul>
                                <li><a href="#">Topik 1 : Introduction</a></li>
                                <li><a href="#">Topik 2 : Setting Environment</a></li>
                                <li><a href="#">Topik 3 : Part 1</a></li>
                                <li><a href="#">Topik 4 : Part 2</a></li>
                                <li><a href="#">Topik 5 : Part 3</a></li>
                                <li><a href="#">Topik 6 : Part 4</a></li>
                                <li><a href="#">Topik 7 : Creating Final Project</a></li>
                            </ul>
                            <p><a href="#" class="btn btn-primary">Lihat Resource</a></p>
                        </div>
                        <div class="probootstrap-image probootstrap-animate" id="abt_video">
                            <a href="https://vimeo.com/45830194" class="btn-video popup-vimeo"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection