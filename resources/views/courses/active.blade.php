@extends('layout.app')

@section('page_title', "Kelas Aktif")

@section('content')
    <section class="probootstrap-section probootstrap-section-colored" id="udx_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1 id="heading">Kelas Aktif</h1>
                </div>
            </div>

            <div class="row">
                @for($i = 0; $i <= 6; $i++)
                    <div class="col-md-3 col-sm-6">
                        <a href="/user_courses_detail">
                            <div class="probootstrap-service-2 text-center probootstrap-animate">
                                <figure class="media">
                                    <img src="img/person_1.jpg" class="img-responsive">
                                </figure>
                                <div class="text">
                                    <h5><b>Membuat Website dengan Laravel</b></h5>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star "></span>
                                    <p>Jon Snow</p>
                                    <p>Topik Selesai : 3/7</p>

                                    <span class="label success"> <10  Topik </span>

                                </div>
                            </div>
                        </a>
                    </div>
                @endfor
                <div class="clearfix visible-sm-block visible-xs-block"></div>
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

@endsection