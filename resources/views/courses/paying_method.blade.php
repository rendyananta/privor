@extends('layout.app')

@section('page_title', "Pembayaran")

@section('content')

    <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1 id="heading">Pembayaran</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section probootstrap-section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row probootstrap-gutter0">
                        <div class="col-md-4" id="probootstrap-sidebar">
                            <div class="probootstrap-sidebar-inner probootstrap-overlap probootstrap-animate">
                                <div class="text-uppercase probootstrap-uppercase">React Native for Beginner</div>
                                <h3>Rp. 560.000</h3>
                                <h4 style="font-size: 12pt">Bayar dengan Virtual Account</h4>
                                <p>Nomor Virtual Account : </p>
                                <h3>049393045204</h3>
                                <a href="{{ route('courses.start') }}" target="_blank" class="btn btn-primary">Selesai
                                    Bayar</a>
                            </div>
                        </div>

                        <div class="col-md-7 col-md-push-1 probootstrap-overlap probootstrap-animate"
                             id="probootstrap-content">
                            <h2 id="description">Ikhtisar</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime rerum possimus maiores,
                                natus fugiat quibusdam dolorem. Dolore, odit ipsum, commodi distinctio repellendus vel
                                tempore accusamus ea assumenda totam aut tempora. Illum sed harum, doloremque magnam
                                nostrum. Fuga tempora corrupti unde ratione, placeat voluptates dicta asperiores est, ad
                                voluptatem dolor, delectus sapiente voluptatum! Rerum magnam ducimus voluptas dolorum
                                consectetur ex facilis quaerat sapiente eaque consequuntur perspiciatis nesciunt tenetur
                                quibusdam corrupti voluptates consequatur repudiandae, quisquam aperiam veniam.
                                Architecto voluptas blanditiis provident modi cumque a eius nam dignissimos numquam
                                ducimus earum odit ipsam, reiciendis assumenda id quasi dolore, totam impedit molestias?
                                Alias, ipsa!</p>
                            <h2 id="description">Silabus</h2>
                            <p>Velit natus alias eligendi architecto rem, itaque distinctio? Excepturi obcaecati fuga
                                ratione. Dolore in ipsam rem ullam nemo error aperiam dolores eius, doloremque
                                blanditiis placeat fugiat libero id atque, recusandae, voluptate laboriosam, distinctio
                                omnis ab. Eius obcaecati, laudantium ex voluptatum voluptas, cum vitae molestiae quam
                                est omnis nulla, quis velit? Esse natus tempore molestias deserunt, tempora illum
                                labore, fuga animi totam dolore doloribus doloremque laudantium velit distinctio, non
                                cumque dolorem delectus quia quibusdam? Amet, consectetur. Reprehenderit unde, eveniet
                                temporibus, quae possimus magni. Dolore quo eveniet, distinctio dicta quisquam? Aliquid
                                libero dolore provident, beatae odit facilis quasi dolorum sit ad minus!</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection