@extends('layout.app')

@section('page_title', "Detail Kelas")

@section('content')

    <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1 id="heading">Membuat Website dengan Laravel</h1>
                    <p id="update">Terakhir diperbarui 3 hari yang lalu</p>
                    <div id="rating"><span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star "></span>
                        <span>Skor : 4/5</span></div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section probootstrap-section-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row probootstrap-gutter0">
                        <div class="col-md-4" id="probootstrap-sidebar">
                            <div class="probootstrap-sidebar-inner probootstrap-overlap probootstrap-animate">
                                <h3 id="heading">Jadwal Privat</h3>
                                <p>Pilih dua jadwal yang berbeda</p>
                                <ul class="row probootstrap-side-menu ">
                                    <li class="col-sm-12"><a href="#" class="label label-plain">Senin 16.00-17.30</a>
                                    </li>
                                    <li class="col-sm-12"><a href="#" class="label label-plain">Senin 19.00-20.30</a>
                                    </li>
                                    <li class="col-sm-12"><a href="#" class="label label-plain">Selasa 16.00-17.30</a>
                                    </li>
                                </ul>

                                <hr>

                                <h3 id="heading">Tutor</h3>
                                <p>Pilih tutor yang tersedia</p>
                                <ul class="row probootstrap-side-menu ">
                                    @for($i=0;$i<=6;$i++)
                                        <li class="col-sm-3"><a href="#" data-toggle="modal"
                                                                data-target="#teacherProfileModal"
                                                                title="Jon Snow"><img src="img/person_1.jpg"
                                                                                      class="img-circle img-responsive"></a>
                                        </li>
                                    @endfor

                                </ul>
                            </div>
                            <a href="{{ route('courses.pay') }}" role="button"
                               class="btn btn-primary btn-lg btn-ghost probootstrap-animate"
                               style="background-image: linear-gradient(to right, #45c46c, #99ea5c);">Bergabung ke Kelas
                                ini</a>
                        </div>

                        <div class="col-md-7 col-md-push-1 probootstrap-overlap probootstrap-animate"
                             id="probootstrap-content">
                            <a href="{{ route('courses.start') }}" class="btn-video popup-vimeo"><img
                                        src="img/assets/image102275.png" class="img-responsive"
                                        title="Kelas Tidak Tersedia saat ini"></a>
                            <h2 id="description">Ikhtisar</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime rerum possimus maiores,
                                natus fugiat quibusdam dolorem. Dolore, odit ipsum, commodi distinctio repellendus vel
                                tempore accusamus ea assumenda totam aut tempora. Illum sed harum, doloremque magnam
                                nostrum. Fuga tempora corrupti unde ratione, placeat voluptates dicta asperiores est, ad
                                voluptatem dolor, delectus sapiente voluptatum! Rerum magnam ducimus voluptas dolorum
                                consectetur ex facilis quaerat sapiente eaque consequuntur perspiciatis nesciunt tenetur
                                quibusdam corrupti voluptates consequatur repudiandae, quisquam aperiam veniam.
                                Architecto voluptas blanditiis provident modi cumque a eius nam dignissimos numquam
                                ducimus earum odit ipsam, reiciendis assumenda id quasi dolore, totam impedit molestias?
                                Alias, ipsa!</p>
                            <h2 id="description">Silabus</h2>
                            <p>Velit natus alias eligendi architecto rem, itaque distinctio? Excepturi obcaecati fuga
                                ratione. Dolore in ipsam rem ullam nemo error aperiam dolores eius, doloremque
                                blanditiis placeat fugiat libero id atque, recusandae, voluptate laboriosam, distinctio
                                omnis ab. Eius obcaecati, laudantium ex voluptatum voluptas, cum vitae molestiae quam
                                est omnis nulla, quis velit? Esse natus tempore molestias deserunt, tempora illum
                                labore, fuga animi totam dolore doloribus doloremque laudantium velit distinctio, non
                                cumque dolorem delectus quia quibusdam? Amet, consectetur. Reprehenderit unde, eveniet
                                temporibus, quae possimus magni. Dolore quo eveniet, distinctio dicta quisquam? Aliquid
                                libero dolore provident, beatae odit facilis quasi dolorum sit ad minus!</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection