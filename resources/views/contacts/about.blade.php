@extends('template.template')

@section('page_title', "Tentang")

@section('content')

    <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1 id="head">About Privor</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="probootstrap-flex-block">
                        <div class="probootstrap-text probootstrap-animate">
                            <div class="text-uppercase probootstrap-uppercase">History</div>
                            <h3>Take A Look at How We Begin</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis explicabo veniam labore
                                ratione illo vero voluptate a deserunt incidunt odio aliquam commodi blanditiis voluptas
                                error non rerum temporibus optio accusantium!</p>
                            <p><a href="#" class="btn btn-primary">Learn More</a></p>
                        </div>
                        <div class="probootstrap-image probootstrap-animate" id="abt_video">
                            <a href="https://vimeo.com/45830194" class="btn-video popup-vimeo"><i
                                        class="icon-play3"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
                    <h2>Get In Touch</h2>
                    <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore
                        natus quos quibusdam soluta at.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="service left-icon probootstrap-animate">
                        <div class="icon"><i class="icon-google-plus"></i></div>
                        <div class="text">
                            <h3>Google+</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore
                                natus quos quibusdam soluta at.</p>
                        </div>
                    </div>
                    <div class="service left-icon probootstrap-animate">
                        <div class="icon"><i class="icon-youtube"></i></div>
                        <div class="text">
                            <h3>Youtube</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore
                                natus quos quibusdam soluta at.</p>
                        </div>
                    </div>
                    <div class="service left-icon probootstrap-animate">
                        <div class="icon"><i class="icon-twitter"></i></div>
                        <div class="text">
                            <h3>Twitter</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore
                                natus quos quibusdam soluta at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service left-icon probootstrap-animate">
                        <div class="icon"><i class="icon-facebook"></i></div>
                        <div class="text">
                            <h3>Facebook</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore
                                natus quos quibusdam soluta at.</p>
                        </div>
                    </div>

                    <div class="service left-icon probootstrap-animate">
                        <div class="icon"><i class="icon-instagram2"></i></div>
                        <div class="text">
                            <h3>Instagram</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore
                                natus quos quibusdam soluta at.</p>
                        </div>
                    </div>

                    <div class="service left-icon probootstrap-animate">
                        <div class="icon"><i class="icon-linkedin"></i></div>
                        <div class="text">
                            <h3>LinkedIn</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore
                                natus quos quibusdam soluta at.</p>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END row -->
        </div>
    </section>

@endsection