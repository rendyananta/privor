@extends('layout.app')

@section('page_title', 'Belajar IT Cepat dan Mudah')

@section('content')

    <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left section-heading probootstrap-animate">
                    <h2 id="heading"><b>Privor</b></h2>
                    <p>Belajar IT privat tidak akan semudah ini</p>
                    <p><a href="#" class="btn btn-primary"
                          style="background: #7676eb; box-shadow: 3px 3px 2px #808080;">Coba Sekarang</a></p>
                </div>
                <div class="col-md-6 text-left probootstrap-image probootstrap-animate">
                    <img src="img/assets/a1.png" class="img-responsive">
                </div>
            </div>
        </div>
    </section>

    <section
            class="probootstrap-section probootstrap-section-colored probootstrap-bg probootstrap-custom-heading probootstrap-tab-section"
            style="background-color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center section-heading probootstrap-animate">
                    <h2 class="mb0" id="heading">"Belajar pemrograman itu sulit jika tidak dengan guru yang tepat!"</h2>
                </div>
            </div>
        </div>
        <hr style="border-width: 3px; margin:3em; border-color: #000" class="probootstrap-animate">

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
                    <div class="probootstrap-counter-wrap">
                        <div class="probootstrap-icon">
                            <i class="icon-location"></i>
                        </div>
                        <div class="probootstrap-text">
                  <span class="probootstrap-counter">
                    <span class="probootstrap-counter-label">Privat coding tanpa ada batasan jarak</span>
                  </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
                    <div class="probootstrap-counter-wrap">
                        <div class="probootstrap-icon">
                            <i class="icon-code"></i>
                        </div>
                        <div class="probootstrap-text">
                  <span class="probootstrap-counter">
                    <span class="probootstrap-counter-label">Live Coding</span>
                  </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-xs-block"></div>
                <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
                    <div class="probootstrap-counter-wrap">
                        <div class="probootstrap-icon">
                            <i class="icon-trophy"></i>

                        </div>
                        <div class="probootstrap-text">
                  <span class="probootstrap-counter">
                    <span class="probootstrap-counter-label">Efektif dan mudah dipahami</span>
                  </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section" id="probootstrap-counter">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left section-heading probootstrap-animate">
                    <h2 id="heading">Kategori</h2>
                    <!-- <p class="lead">Sed a repudiandae impedit voluptate nam Deleniti dignissimos perspiciatis nostrum porro nesciunt</p> -->
                </div>
                <div class="col-md-6 text-right section-heading probootstrap-animate">
                    <h4 style="font-size: 16pt;"><a href="/kelas">Lihat Semua</a></h4>
                    <!-- <p class="lead">Sed a repudiandae impedit voluptate nam Deleniti dignissimos perspiciatis nostrum porro nesciunt</p> -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="probootstrap-teacher text-center probootstrap-animate">
                        <figure class="media">
                            <a href="#"><img src="img/assets/android.png" class="img-responsive"></a>
                        </figure>
                        <div class="text">
                            <h3>Android</h3>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="probootstrap-teacher text-center probootstrap-animate">
                        <figure class="media">
                            <a href="#"><img src="img/assets/apple.png" class="img-responsive"></a>
                        </figure>
                        <div class="text">
                            <h3>iOS</h3>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="probootstrap-teacher text-center probootstrap-animate">
                        <figure class="media">
                            <a href="#"><img src="img/assets/global.png" class="img-responsive"></a>
                        </figure>
                        <div class="text">
                            <h3>Website</h3>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="probootstrap-teacher text-center probootstrap-animate">
                        <figure class="media">
                            <a href="#"><img src="img/assets/brand.png" class="img-responsive"></a>
                        </figure>
                        <div class="text">
                            <h3>Design</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section probootstrap-animate" style="background-color: rgba(118,118,235,.2); ">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left section-heading probootstrap-animate">
                    <h2 id="heading"><strong>Kelas</strong></h2>
                </div>
                <div class="col-md-6 text-right section-heading probootstrap-animate">
                    <h4 style="font-size: 16pt;"><a href="/kelas">Lihat Semua</a></h4>
                </div>
            </div>
            <!-- END row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div id="featured-news" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="owl-carousel" id="owl1">
                                        <div class="item">
                                            <a href="#" class="probootstrap-featured-news-box">
                                                <figure class="probootstrap-media"><img src="img/img_sm_3.jpg"
                                                                                        alt="Free Bootstrap Template by uicookies.com"
                                                                                        class="img-responsive"></figure>
                                                <div class="probootstrap-text">
                                                    <h3>Belajar Java mudah dan cepat</h3>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star "></span>
                                                    <p>Rio Ikhsan</p>
                                                    <span class="label danger"> 10-14  Topik </span>
                                                    <span class="label plain">Rp. 425.000</span>
                                                </div>
                                            </a>
                                        </div>
                                        <!-- END item -->
                                        <div class="item">
                                            <a href="#" class="probootstrap-featured-news-box">
                                                <figure class="probootstrap-media"><img src="img/img_sm_1.jpg"
                                                                                        alt="Free Bootstrap Template by uicookies.com"
                                                                                        class="img-responsive"></figure>
                                                <div class="probootstrap-text">
                                                    <h3>Belajar Java mudah dan cepat</h3>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star "></span>
                                                    <p>Rio Ikhsan</p>
                                                    <span class="label success"> 10-14  Topik </span>
                                                    <span class="label plain">Rp. 425.000</span>

                                                </div>
                                            </a>
                                        </div>
                                        <!-- END item -->
                                        <div class="item">
                                            <a href="#" class="probootstrap-featured-news-box">
                                                <figure class="probootstrap-media"><img src="img/img_sm_2.jpg"
                                                                                        alt="Free Bootstrap Template by uicookies.com"
                                                                                        class="img-responsive"></figure>
                                                <div class="probootstrap-text">
                                                    <h3>Belajar Java mudah dan cepat</h3>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star "></span>
                                                    <p>Rio Ikhsan</p>
                                                    <span class="label info"> 10-14  Topik </span>
                                                    <span class="label plain">Rp. 425.000</span>

                                                </div>
                                            </a>
                                        </div>
                                        <!-- END item -->
                                        <div class="item">
                                            <a href="#" class="probootstrap-featured-news-box">
                                                <figure class="probootstrap-media"><img src="img/img_sm_3.jpg"
                                                                                        alt="Free Bootstrap Template by uicookies.com"
                                                                                        class="img-responsive"></figure>
                                                <div class="probootstrap-text">
                                                    <h3>Belajar Java mudah dan cepat</h3>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star "></span>
                                                    <p>Rio Ikhsan</p>
                                                    <span class="label warning"> 10-14  Topik </span>
                                                    <span class="label plain">Rp. 425.000</span>

                                                </div>
                                            </a>
                                        </div>
                                        <!-- END item -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END row -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-cta"
             style="background-image: url('{{asset('img/assets/rsz_11a3.png') }}'); background-repeat: no-repeat;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="probootstrap-animate" data-animate-effect="fadeInRight">Tertarik menjadi pengajar?</h4>
                </div>
                <div class="col-md-6">
                    <a href="#" role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate"
                       data-animate-effect="fadeInRight"
                       style="background-image: linear-gradient(to left, #3b63ff, #bc7bff);">Mendaftar</a>
                    <a href="#" role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate"
                       data-animate-effect="fadeInRight"
                       style="background-image: linear-gradient(to right, #45c46c, #99ea5c);">Masuk</a>
                </div>
            </div>
        </div>
    </section>

@endsection