@if($status == 'Dibayar')
    <form action="{{ route('admin.payments.unverif', ['payment' => $id]) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        <button class="btn btn-sm btn-primary" onclick="return confirm('Batalkan verifikasi pembayaran transaksi ?')">
            UNVERIF
        </button>
    </form>
@elseif ($status == 'Menunggu verifikasi')
    <form action="{{ route('admin.payments.verif', ['payment' => $id]) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        <button class="btn btn-sm btn-primary" onclick="return confirm('Verifikasi pembayaran transaksi ?')">
            VERIF
        </button>
    </form>
@endif

<form action="{{ route('admin.payments.destroy', ['payment' => $id]) }}" method="post">
    {!! csrf_field() !!}
    {!! method_field('delete') !!}

    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
        <i class="icons ion-md-trash"></i>
    </button>
</form>
