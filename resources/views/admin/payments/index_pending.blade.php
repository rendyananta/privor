@extends('admin.layouts.app')
@section('page_title', 'Transaksi')


@section('breadcrumb')
    <li class="breadcrumb-item active">List Transaksi Pending</li>
@endsection

@section('body')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>List Transaksi Pending</h3>

                <ul class="nav nav-tabs pt-4" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" aria-selected="false" href="{{ route('admin.payments.index') }}">
                            Sukses
                            <i class="ion-md-checkmark"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-selected="false" href="{{ route('admin.payments.unverified') }}">
                            Menunggu Verifikasi
                            <i class="ion-md-warning"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-selected="true" href="{{ route('admin.payments.pending') }}">
                            Menunggu Pembayaran
                            <i class="ion-md-clock"></i>
                        </a>
                    </li>
                </ul>

                <div class="pt-3">
                    {!! $dataTable->table(['class' => 'table table-bordered']) !!}
                </div>

            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
@endsection

@push('foots') {!! $dataTable->scripts() !!} @endpush