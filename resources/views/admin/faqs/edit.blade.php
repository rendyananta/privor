@extends('admin.layouts.app')

@section('page_title', 'Edit Tanya Jawab')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.faqs.index') }}">List Tanya Jawab</a>
    </li>
    <li class="breadcrumb-item active">Edit Tanya Jawab {{ $faq->name }}</li>
@endsection

@push('heads')
    <script src="{{ asset('js/ckeditor.js') }}"></script>
@endpush

@section('body')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h3>Edit Tanya Jawab {{ $faq->question }}</h3>

                    <form class="pt-2" action="{{ route('admin.categories.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Pertanyaan</label>
                            <input type="text" name="question"  class="form-control {{ set_error('name') }}" value="{{ $faq->question }}">
                            {!! get_error('question') !!}
                        </div>

                        <div class="form-group">
                            <label for="">Ikon</label>
                            <textarea name="answer" id="ckeditor" cols="30" rows="10">
                                {!! $faq->answer !!}
                            </textarea>
                            <input type="file" name="answer"  class="{{ set_error('answer') }}">
                            {!! get_error('answer') !!}
                        </div>
                        <button class="btn btn-warning">
                            <span class="icon ion-md-create"></span> Simpan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('foots')
    <script>
        ClassicEditor
            .create(document.querySelector( '#ckeditor' ))
    </script>
@endpush