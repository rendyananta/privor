@extends('admin.layouts.app')

@section('page_title', 'Tanya Jawab Baru')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.faqs.index') }}">List Tanya Jawab</a>
    </li>
    <li class="breadcrumb-item active">Tanya Jawab Baru</li>
@endsection

@push('heads')
    <script src="{{ asset('js/ckeditor.js') }}"></script>
@endpush

@section('body')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h3>Tanya Jawab Baru</h3>

                    <form class="pt-2" action="{{ route('admin.faqs.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Pertanyaan</label>
                            <input type="text" name="question"  class="form-control {{ set_error('question') }}" value="{{ old('question') }}">
                            {!! get_error('question') !!}
                        </div>

                        <div class="form-group">
                            <label for="">Jawab</label>
                            <textarea name="answer" id="ckeditor" cols="30" rows="40" class="form-control">
                                {!! old('answer') !!}
                            </textarea>
                            {!! get_error('answer') !!}
                        </div>
                        <button class="btn btn-success">
                            <span class="icon ion-md-add"></span> Tambah
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('foots')
    <script>
        ClassicEditor
            .create(document.querySelector( '#ckeditor' ))
    </script>
@endpush