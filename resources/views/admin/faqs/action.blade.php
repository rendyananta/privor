<form action="{{ route('admin.faqs.destroy', ['faq' => $id]) }}" method="post">
    {!! csrf_field() !!}
    {!! method_field('delete') !!}

    <a href="{{ route('admin.faqs.show', ['faq' => $id]) }}" class="btn btn-sm btn-primary">
        <i class="ion-md-eye"></i>
    </a>

    <a href="{{ route('admin.faqs.edit', ['faq' => $id]) }}" class="btn btn-sm btn-warning">
        <i class="ion-md-create"></i>
    </a>

    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
        <i class="icons ion-md-trash"></i>
    </button>
</form>
