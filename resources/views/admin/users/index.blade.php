@extends('admin.layouts.app')

@section('page_title', 'Pengguna')

@section('breadcrumb')
    <li class="breadcrumb-item active">List Pengguna</li>
@endsection

@section('body')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>List Pengguna</h3>

                <div class="pt-3">
                    {!! $dataTable->table(['class' => 'table table-bordered']) !!}
                </div>

            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
@endsection

@push('foots') {!! $dataTable->scripts() !!} @endpush