<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    <title>Administrator Login Page</title>
</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card-group">
                <div class="card p-4">

                    @include('admin.layouts.partials.errors_alert')

                    <form class="card-body" method="post" action="{{ route('admin.login') }}">
                        {{ csrf_field() }}
                        <h1>Masuk</h1>
                        <p class="text-muted">Masuk untuk melanjutkan</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="icon ion-md-person"></i>
                                </span>
                            </div>
                            <input class="form-control {{ set_error('password') }}" type="text" name="username" placeholder="Username">
                            {!! get_error('username') !!}
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="icon ion-md-lock"></i>
                                </span>
                            </div>
                            <input class="form-control {{ set_error('password') }}" type="password" name="password" placeholder="Password">
                            {!! get_error('password') !!}
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-primary px-4">
                                    Masuk
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/dashboard.js') }}"></script>
</body>
</html>