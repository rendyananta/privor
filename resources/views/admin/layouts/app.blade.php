<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Administrator - @yield('page_title')</title>
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    @stack('heads')
</head>
<body class="app header-fixed sidebar-fixed sidebar-show">

    <div class="app-header">
        @include('admin.layouts.partials.navbar')
    </div>
    <div class="app-body">
        @include('admin.layouts.partials.sidebar')

        <main class="main">
            {{-- Breadcrumb --}}
            @include('admin.layouts.partials.breadcrumb')

            <div class="container-fluid">
                <div class="animated fadeIn">

                    <div class="py-3">
                        @include('admin.layouts.partials.errors_alert')
                    </div>

                    @yield('body')
                </div>
            </div>

        </main>
    </div>
    <script src="{{ asset('js/dashboard.js') }}"></script>
    @stack('foots')
</body>
</html>