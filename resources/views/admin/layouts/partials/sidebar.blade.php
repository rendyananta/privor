<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">Utama</li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.dashboard.index') }}">
                    <i class="icon ion-md-trending-up"></i> Dashboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.payments.index') }}">
                    <i class="icon ion-md-swap"></i> Transaksi
                    <span class="badge badge-primary">NEW</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.users.index') }}">
                    <i class="icon ion-md-person"></i> Pengguna
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.lecturers.index') }}">
                    <i class="icon ion-md-school"></i> Pengajar
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.courses.index') }}">
                    <i class="icon ion-md-laptop"></i> Kelas
                </a>
            </li>
            <li class="nav-title">Pengaturan Web</li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.categories.index') }}">
                    <i class="icon ion-md-filing"></i> Kategori
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.faqs.index') }}">
                    <i class="icon ion-md-chatboxes"></i> Tanya Jawab
                </a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon ion-md-information-circle"></i> Tentang
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="">
                            <i class="icon ion-md-contacts"></i> Kontak
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>