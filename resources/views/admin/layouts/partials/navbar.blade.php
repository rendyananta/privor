<a class="navbar-brand" href="#">
    <img src="{{ asset('brand.svg') }}" class="brand" alt="">
</a>
<div class="collapse navbar-collapse" id="navbarNav">
    <header class="navbar navbar-light">
        <button class="navbar-toggler sidebar-toggler" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
    </header>
    <ul class="navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="#">Dasbor<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Topik</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Pricing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
        </li>
    </ul>
</div>