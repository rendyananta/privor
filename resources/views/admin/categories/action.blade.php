<form action="{{ route('admin.categories.destroy', ['category' => $id]) }}" method="post">
    {!! csrf_field() !!}
    {!! method_field('delete') !!}

    <a href="{{ route('admin.categories.show', ['category' => $id]) }}" class="btn btn-sm btn-primary">
        <i class="ion-md-eye"></i>
    </a>

    <a href="{{ route('admin.categories.edit', ['category' => $id]) }}" class="btn btn-sm btn-warning">
        <i class="ion-md-create"></i>
    </a>

    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
        <i class="icons ion-md-trash"></i>
    </button>
</form>
