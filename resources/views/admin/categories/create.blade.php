@extends('admin.layouts.app')

@section('page_title', 'Buat kategori baru')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.categories.index') }}">List Kategori</a>
    </li>
    <li class="breadcrumb-item active">Kategori baru</li>
@endsection

@section('body')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h3>Kategori Baru</h3>

                    <form class="pt-2" action="{{ route('admin.categories.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" name="name"  class="form-control {{ set_error('name') }}" value="{{ old('name') }}">
                            {!! get_error('name') !!}
                        </div>
                        <div class="form-group">
                            <label for="">Ikon</label>
                            <input type="file" name="icon"  class="{{ set_error('icon') }}">
                            {!! get_error('icon') !!}
                        </div>
                        <button class="btn btn-success">
                            <span class="icon ion-md-add"></span> Tambah
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection