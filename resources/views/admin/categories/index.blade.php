@extends('admin.layouts.app')

@section('page_title', 'Kategori')

@section('breadcrumb')
    <li class="breadcrumb-item active">List Kategori</li>
@endsection

@section('body')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>List Kategori</h3>

                <a href="{{ route('admin.categories.create') }}" class="btn btn-primary pull-right pt-2">
                    <i class="icon ion-md-add"></i> Baru
                </a>

                <div class="pt-3">
                    {!! $dataTable->table(['class' => 'table table-bordered']) !!}
                </div>

            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
@endsection

@push('foots') {!! $dataTable->scripts() !!} @endpush