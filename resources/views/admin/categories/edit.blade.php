@extends('admin.layouts.app')

@section('page_title', 'Edit Kategori')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.categories.index') }}">List Kategori</a>
    </li>
    <li class="breadcrumb-item active">Edit Kategori {{ $category->name }}</li>
@endsection

@section('body')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h3>Edit Kategori {{ $category->name }}</h3>

                    <form class="pt-2" action="{{ route('admin.categories.update', ['category' => $category]) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" name="name"  class="form-control {{ set_error('name') }}" value="{{ $category->name }}">
                            {!! get_error('name') !!}
                        </div>
                        @if(isset($category->icon))
                        <div class="form-group">
                            <img src="{{ storage_asset($category->icon) }}" width="60" alt="">
                        </div>
                            <p>Pilih file untuk mengubah ikon</p>
                        @endif
                        <div class="form-group">
                            <label for="">Ikon</label>
                            <input type="file" name="icon"  class="{{ set_error('icon') }}">
                            {!! get_error('icon') !!}
                        </div>
                        <button class="btn btn-warning">
                            <span class="icon ion-md-create"></span> Simpan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection