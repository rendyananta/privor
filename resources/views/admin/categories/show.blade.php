@extends('admin.layouts.app')

@section('page_title', 'Detil Kategori')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.categories.index') }}">List Kategori</a>
    </li>
    <li class="breadcrumb-item active">Detil Kategori {{ $category->name }}</li>
@endsection

@section('body')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h3>{{ $category->name }}</h3>
                    @if (isset($category->icon))
                        <img src="{{ storage_asset($category->icon) }}" class="img-circle" width="80" alt="">
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection