@extends('admin.layouts.app')

@section('page_title', 'Pengajar Belum Terverifikasi')

@section('breadcrumb')
    <li class="breadcrumb-item active">List Pengajar Belum Terverifikasi</li>
@endsection

@section('body')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>List Pengajar Belum Terverifikasi</h3>

                <ul class="nav nav-tabs pt-4" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link accordion" aria-selected="false" href="{{ route('admin.lecturers.index') }}">
                            Terverifikasi
                            <i class="ion-md-checkmark"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-selected="true" href="{{ route('admin.lecturers.unverified') }}">
                            Perlu Verifikasi
                            <i class="ion-md-warning"></i>
                        </a>
                    </li>
                </ul>

                <div class="pt-3">
                    {!! $dataTable->table(['class' => 'table table-bordered']) !!}
                </div>

            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
@endsection

@push('foots') {!! $dataTable->scripts() !!} @endpush