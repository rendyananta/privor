@if($is_verified)
    <form action="{{ route('admin.lecturers.unverif', ['lecturer' => $id]) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        <button class="btn btn-sm btn-primary" onclick="return confirm('Batalkan verifikasi pengajar ?')">
            UNVERIF
        </button>
    </form>
@else
    <form action="{{ route('admin.lecturers.verif', ['lecturer' => $id]) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        <button class="btn btn-sm btn-primary" onclick="return confirm('Verifikasi pengajar ?')">
            VERIF
        </button>
    </form>
@endif

<form action="{{ route('admin.lecturers.destroy', ['lecturer' => $id]) }}" method="post">
    {!! csrf_field() !!}
    {!! method_field('delete') !!}

    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
        <i class="icons ion-md-trash"></i>
    </button>
</form>
