<form action="{{ route('admin.courses.destroy', ['course' => $id]) }}" method="post">
    {!! csrf_field() !!}
    {!! method_field('delete') !!}

    <a href="{{ route('admin.courses.show', ['course' => $id]) }}" class="btn btn-sm btn-primary">
        <i class="ion-md-eye"></i>
    </a>

    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
        <i class="icons ion-md-trash"></i>
    </button>
</form>
