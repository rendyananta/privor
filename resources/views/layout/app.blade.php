<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Privor - @yield('page_title')</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{ asset('pre-compiled/styles-merged.css') }}">
    <link rel="stylesheet" href="{{ asset('pre-compiled/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pre-compiled/custom.css') }}">

</head>

<body>
<div class="probootstrap-page-wrapper">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default probootstrap-navbar">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/main" title="Privor">Privor</a>
            </div>
            <div id="navbar-collapse" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('home.landing') }}">Home</a></li>
                    <li><a href="{{ route('courses.index') }}">Kelas</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#loginModal">Masuk</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#signupModal">Mendaftar</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <footer class="probootstrap-footer probootstrap-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-3">
                    <div class="probootstrap-footer-widget">
                        <img title="Privor" src="img/rsz_1rsz_privor-txt-logo.png">
                        <ul>
                            <li><a href="/tentang">Tentang</a></li>
                            <li><a href="/eula">Syarat dan Ketentuan</a></li>
                            <li><a href="/privacy">Kebijakan Privasi</a></li>
                            <li><a href="#">Temukan Kami di</a></li>
                            <ul class="probootstrap-footer-social">
                                <li><a href="#" title="Google Plus"><i class="icon-google-plus"></i></a></li>
                                <li><a href="#" title="Youtube"><i class="icon-youtube"></i></a></li>
                                <li><a href="#" title="Twitter"><i class="icon-twitter"></i></a></li>
                                <li><a href="#" title="Instagram"><i class="icon-instagram2"></i></a></li>
                                <li><a href="#" title="Facebook"><i class="icon-facebook"></i></a></li>
                                <li><a href="#" title="Linkedin"><i class="icon-linkedin"></i></a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-3">
                    <div class="probootstrap-footer-widget">
                        <h3>PEMBELAJARAN</h3>
                        <ul>
                            <li><a href="/metode">Metode Pembelajaran</a></li>
                            <li><a href="/signup_teacher">Bergabung Menjadi Pengajar</a></li>
                            <li><a href="/guide">Cara Mengambil kelas</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-3">
                    <div class="probootstrap-footer-widget">
                        <h3>TERKAIT</h3>
                        <ul>
                            <li><a href="/teacher">Profil Pengajar</a></li>
                            <li><a href="#">Kategori</a></li>
                            <li><a href="/kelas">Kelas</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END row -->

        </div>

        <hr>

        <div class="probootstrap-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-left">
                        <p>&copy; 2018 Privor</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div>

<!-- Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">Masuk</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/user_index" enctype="multipart/form-data">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" autocomplete="on" required>
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" autocomplete="on" required>
                        <span class="fa fa-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox"> Ingat saya
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <input type="submit" class="btn btn-primary btn-block btn-flat" value="Masuk">
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Signup Modal -->
<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">Mendaftar</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Nama" required>
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" required>
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" autocomplete="on" required>
                        <span class="fa fa-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Ketik Ulang Password" autocomplete="on"
                               required>
                        <span class="fa fa-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox"> Saya setuju dengan <a href="" data-toggle="modal"
                                                                                  data-target="">Aturan Penggunaan dan
                                        Kebijakan Privasi</a>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Mendaftar</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('pre-compiled/scripts.min.js') }}"></script>
<script src="{{ asset('pre-compiled/main.min.js') }}"></script>
<script src="{{ asset('pre-compiled/custom.js') }}"></script>
</body>
</html>
