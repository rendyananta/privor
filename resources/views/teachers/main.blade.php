@extends('layout.app')

@section('page_title', "Tutor")

@section('content')

    <section class="probootstrap-section probootstrap-section-colored" style="padding-top: 20px; padding-bottom: 0px;">
        <div class="container">
            <div class="col-sm-12">
                <div class="col-md-6 text-left section-heading probootstrap-animate">
                    <h1 id="heading">Kelas</h1>
                </div>
                <div class="col-md-6 text-right section-heading probootstrap-animate">
                    <a href="#" role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate"
                       data-toggle="modal" data-target="#createClassModal" id="btn_enroll">Buat Kelas Baru</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="/teacher_detail_class">
                        <div class="probootstrap-service-2 text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_1.jpg" class="img-responsive">
                            </figure>
                            <div class="text">
                                <h5><b>Membuat Website dengan Laravel</b></h5>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star "></span>
                                <p>Jon Snow</p>
                                <span class="label success"> 12  Topik </span>
                                <span class="label plain">8 Peserta</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="/teacher_detail_class">
                        <div class="probootstrap-service-2 text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_1.jpg" class="img-responsive">
                            </figure>
                            <div class="text">
                                <h5><b>Membangun Aplikasi Simple dengan React Native</b></h5>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star "></span>
                                <p>Jon Snow</p>
                                <span class="label danger"> 14  Topik </span>
                                <span class="label plain">3 Peserta</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="/teacher_detail_class">
                        <div class="probootstrap-service-2 text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_1.jpg" class="img-responsive">
                            </figure>
                            <div class="text">
                                <h5><b> Belajar React Native untuk Pemula</b></h5>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star "></span>
                                <p>Jon Snow</p>
                                <span class="label warning"> 24  Topik </span>
                                <span class="label plain">8 Peserta</span>
                            </div>
                        </div>
                    </a>
                </div>

                @for ($i = 0; $i < 8; $i++)
                    <div class="col-md-3 col-sm-6">
                        <a href="/teacher_detail_class">
                            <div class="probootstrap-service-2 text-center probootstrap-animate">
                                <figure class="media">
                                    <img src="img/person_1.jpg" class="img-responsive">
                                </figure>
                                <div class="text">
                                    <h5><b>Belajar Java mudah dan cepat</b></h5>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star "></span>
                                    <p>Jon Snow</p>
                                    <span class="label info"> >20  Topik </span>
                                    <span class="label plain">6 Peserta</span>
                                </div>
                            </div>
                        </a>
                    </div>
                @endfor

                <div class="clearfix visible-sm-block visible-xs-block"></div>
            </div>

            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

    <!-- Create Class Modal -->
    <div class="modal fade" id="createClassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Mendaftar</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Nama Kelas" required>
                            <span class="fa fa-home form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Kapasitas Kelas" required>
                            <span class="fa fa-user form-control-feedback"></span>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                                <option>--Pilih Kategori--</option>
                                <option>Web</option>
                                <option>Android</option>
                                <option>iOS</option>
                                <option>Network</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                                <option>--Pilih Kurikulum--</option>
                                <option>Buat Kurikulum Baru</option>
                                <option>Ikuti Kurikulum Lama</option>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="number" class="form-control" placeholder="Jumlah Topik" required>
                            <span class="fa fa-pencil form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Topik 1" required>
                            <span class="fa fa-tasks form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Topik 1" required>
                            <span class="fa fa-tasks form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Topik 1" required>
                            <span class="fa fa-tasks form-control-feedback"></span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Resource Pendukung</label>
                            <input type="file" id="exampleInputFile">

                            <p class="help-block">Pilih resource untuk kelas baru</p>
                        </div>

                        <div class="row">
                            <div class="col-xs-8">

                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Buat Kelas</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection