@extends('layout.app')

@section('page_title', "Detail Kelas")

@section('content')

    <section class="probootstrap-section probootstrap-section-colored" style="margin-bottom: 7em">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1 id="heading">Membuat Website dengan Laravel</h1>
                    <span>Kapasitas : 16/18</span>
                    <div id="rating"><span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star "></span>
                        <span>Skor : 4/5</span></div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="probootstrap-flex-block">
                        <div class="probootstrap-text probootstrap-animate">
                            <h3 id="heading">Jadwal Privat</h3>
                            <p>Pilih jadwal untuk melihat peserta</p>
                            <ul class="row probootstrap-side-menu ">
                                <li class="col-sm-12"><a href="#" role="button" class="btn btn-primary btn-sm"
                                                         id="btn_jadwal">Senin 16.00-17.30</a></li>
                                <li class="col-sm-12"><a href="#" role="button" class="btn btn-primary btn-sm"
                                                         id="btn_jadwal">Senin 19.00-20.30</a></li>
                                <li class="col-sm-12"><a href="#" role="button" class="btn btn-primary btn-sm"
                                                         id="btn_jadwal">Selasa 16.00-17.30</a></li>
                            </ul>

                            <hr>

                            <h3 id="heading">Siswa</h3>
                            <p>Peserta dalam Kelas</p>
                            <ul class="row probootstrap-side-menu ">
                                @for($i=0;$i<=6;$i++)
                                    <li class="col-sm-3"><a href="#" title="Clarke"><img src="img/person_1.jpg"
                                                                                         class="img-circle img-responsive"></a>
                                    </li>
                                @endfor

                            </ul>
                        </div>
                        <div class="probootstrap-image probootstrap-animate" id="abt_video">
                            <a href="https://vimeo.com/45830194" class="btn-video popup-vimeo" title="Mulai Kelas"><i
                                        class="fa fa-video-camera"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8 col-md-offset-4 probootstrap-overlap probootstrap-animate" id="probootstrap-content"
                 style="margin-top: 3em">
                <h2 id="description">Progress Pembelajaran</h2>
                <div class="row">
                    <div class="col-sm-6" id="std_progress">
                        <a href="#"><span class="label label-success">Tuntas</span>
                            <p>Fuga tempora corrupti unde ratione,</p></a>
                    </div>
                    <div class="col-sm-6" id="std_progress">
                        <a href="#"><span class="label label-success">Tuntas</span>
                            <p>Fuga tempora corrupti unde ratione,</p></a>
                    </div>
                    <div class="col-sm-6" id="std_progress">
                        <a href="#"><span class="label label-success">Tuntas</span>
                            <p>Fuga tempora corrupti unde ratione,</p></a>
                    </div>
                    @for($i=0;$i<=4;$i++)
                        <div class="col-sm-6" id="std_progress">
                            <a href="#"><span class="label label-danger">Tuntas</span>
                                <p>Fuga tempora corrupti unde ratione,</p></a>
                        </div>
                    @endfor
                </div>

                <h2 id="description">Silabus</h2>
                <p>Velit natus alias eligendi architecto rem, itaque distinctio? Excepturi obcaecati fuga ratione.
                    Dolore in ipsam rem ullam nemo error aperiam dolores eius, doloremque blanditiis placeat fugiat
                    libero id atque, recusandae, voluptate laboriosam, distinctio omnis ab. Eius obcaecati, laudantium
                    ex voluptatum voluptas, cum vitae molestiae quam est omnis nulla, quis velit? Esse natus tempore
                    molestias deserunt, tempora illum labore, fuga animi totam dolore doloribus doloremque laudantium
                    velit distinctio, non cumque dolorem delectus quia quibusdam? Amet, consectetur. Reprehenderit unde,
                    eveniet temporibus, quae possimus magni. Dolore quo eveniet, distinctio dicta quisquam? Aliquid
                    libero dolore provident, beatae odit facilis quasi dolorum sit ad minus!</p>

            </div>
        </div>
    </section>




@endsection