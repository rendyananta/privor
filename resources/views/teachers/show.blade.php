@extends('template.template')

@section('page_title', "Profil Pengajar")

@section('content')

    <section class="probootstrap-section probootstrap-section-colored" id="teacherTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left section-heading probootstrap-animate">
                    <h1>Profil Pengajar</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section probootstrap-section-sm" id="teacherContainer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <a href="#">
                        <div class="probootstrap-teacher text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_1.jpg" class="img-responsive">
                            </figure>
                            <div class="text">
                                <h3>John Doe</h3>
                                <p>Programming</p>
                                <p>Rating :
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star "></span></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#">
                        <div class="probootstrap-teacher text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_2.jpg" alt="Free Bootstrap Template by uicookies.com"
                                     class="img-responsive">
                            </figure>
                            <div class="text">
                                <h3>Chris Worth</h3>
                                <p>Networking</p>
                                <p>Rating :
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star "></span></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#">
                        <div class="probootstrap-teacher text-center probootstrap-animate">
                            <figure class="media">
                                <img src="img/person_3.jpg" alt="Free Bootstrap Template by uicookies.com"
                                     class="img-responsive">
                            </figure>
                            <div class="text">
                                <h3>Jane Doe</h3>
                                <p>Design</p>
                                <p>Rating :
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star "></span></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

@endsection