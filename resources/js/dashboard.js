import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap';
import 'datatables.net';
import 'datatables.net-bs4';

window.$ = window.jQuery = $;
window.Popper = Popper;